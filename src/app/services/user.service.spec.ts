import { TestBed } from '@angular/core/testing';

import { FormsProjectService } from './user.service';

describe('FormsProjectService', () => {
  let service: FormsProjectService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FormsProjectService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
